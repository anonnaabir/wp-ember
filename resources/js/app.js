import './bootstrap';

import Alpine from 'alpinejs';
import Vue from 'vue';
import Index from './index.vue';

// vuejs initiate to div
new Vue({
    el: '#app',
    render: h => h(Index),
});


window.Alpine = Alpine;

Alpine.start();
