<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demo_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->string('demo_name');
            $table->string('filepath',255)->nullable();
            $table->string('preview_link',255)->nullable();
            $table->string('thumbnail',255)->nullable();
            $table->string('type')->nullable();
            $table->string('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demo_data');
    }
};
